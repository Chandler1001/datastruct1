/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01universalarrays;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.Random;
import static jdk.nashorn.tools.ShellFunctions.input;

/**
 *
 * @author Dell
 */
public class Day01UniversalArrays {

    /**
     * @param args the command line arguments
     */
    static Scanner input = new Scanner(System.in);
    static Random random = new Random();

    public static void main(String[] args) {
        // TODO code application logic here
        int row;
        int column;

        try {
            System.out.print("How long is your row? ");
            row = input.nextInt();
            System.out.print("How long is your column? ");
            column = input.nextInt();
            int data2d[][] = new int[row][column];

            inputArray(data2d);
            printArray(data2d, column, row);
        } catch (InputMismatchException ex) {
            System.out.println("Error: you must provide an integer number");
            return; // end program
        }
        int[][] arr2 = {
            {12, 56, 986},
            {1, 5, 86},
            {3, 83, 26, 5},};
        int[][] arr1 = {
            {12, 56, 5},
            {5, 896},};
        int[] dup = findDuplicates(arr1, arr2);
        
        int[] arr3 = {12, 56, 5};
        int[] arr4 = {121, 156, 51};
        
        int[] joinarr = join(arr3, arr4);

    }

    static int[] join(int[] a1, int[] a2) {
        int[] joinArr = new int[a1.length+a2.length];
        for (int i = 0; i <joinArr.length ; i++) {
            if(i<a1.length){
                joinArr[i]=a1[i];
            }else{
                joinArr[i]=a2[i-a1.length];
            }
        }
        return joinArr;
    }

    static int[] findDuplicates(int[][] a1, int[][] a2) {
        ArrayList<Integer> lis1 = new ArrayList<>();
        ArrayList<Integer> lis2 = new ArrayList<>();
        ArrayList<Integer> duplicateList = new ArrayList<>();
        /*Array to ArrayList conversion*/
        for (int row = 0; row < a1.length; row++) {
            for (int col = 0; col < a1[row].length; col++) {
                lis1.add(a1[row][col]);
            }
        }
        for (int row = 0; row < a2.length; row++) {
            for (int col = 0; col < a2[row].length; col++) {
                lis2.add(a2[row][col]);
            }
        }
        int count = 0;
        for (int item : lis1) {
            if (lis2.contains(item) && !duplicateList.contains(item)) {
                duplicateList.add(item);
                count++;
            }
        }
        int[] dupArr = new int[count];
        for (int counter = 0; counter < duplicateList.size(); counter++) {
            dupArr[counter] = duplicateList.get(counter);
        }
        System.out.println(duplicateList);
        return dupArr;
    }

    static void inputArray(int[][] data2d) {
        for (int row = 0; row < data2d.length; row++) {
            for (int col = 0; col < data2d[row].length; col++) {
                System.out.print("Enter value row " + (row + 1) + " column " + (col + 1) + ":");
                data2d[row][col] = input.nextInt();
            }
        }
        for (int row = 0; row < data2d.length; row++) {
            for (int col = 0; col < data2d[row].length; col++) {
                System.out.print((col == 0 ? "" : ",") + data2d[row][col]);
            }
            System.out.println("");
        }
    }

    static void printArray(int[][] data2d, int col, int row) {

        ArrayList<Integer> lengthList = new ArrayList<>();
        for (int j = 0; j < col; j++) {
            int lengthMax = 0;
            for (int i = 0; i < row; i++) {

                if (getLength(data2d[i][j]) > lengthMax) {
                    lengthMax = getLength(data2d[i][j]);
                };
            }
            lengthList.add(lengthMax);
        }//System.out.printf("%8d",23); 
        System.out.println(lengthList);
        for (int i = 0; i < row; i++) {

            for (int j = 0; j < col; j++) {
                int maxLength = lengthList.get(j);

                if (j == 0) {
                    System.out.printf("%" + maxLength + "d", data2d[i][j]);
                } else {
                    System.out.printf("%2s%" + maxLength + "d", ", ", data2d[i][j]);
                }
                //System.out.printf((j == 0 ? ("%"+maxLength+"d") : ("%2s%" + maxLength + "d", ", ")) ,data2d[i][j]);
                //System.out.printf((j == 0 ? "%"+maxLength+"d" : "%2s%" + maxLength + "d", ", "), data2d[i][j]);
            }
            System.out.println("");
        }
    }

    static int getLength(int a) {
        Integer a1 = a;
        return a1.toString().length();
    }
}
