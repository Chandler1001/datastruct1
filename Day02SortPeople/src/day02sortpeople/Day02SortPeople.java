package day02sortpeople;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Person implements Comparable<Person> {

    public Person(String name, int age, double heightMeters) {
        this.name = name;
        this.age = age;
        this.heightMeters = heightMeters;
    }

    String name;
    int age;
    double heightMeters;

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + ", heightMeters=" + heightMeters + '}';
    }

    @Override
    public int compareTo(Person other) {
        return name.compareTo(other.name);
    }

    static final Comparator<Person> comparatorByAge = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            return p1.age - p2.age;
        }
    };

    static final Comparator<Person> comparatorByAgeLambdaExample = (Person p1, Person p2) -> p1.age - p2.age;

    static final Comparator<Person> comparatorByHeight = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            // GOOD ALTERNATIVE: return Double.compare(p1.heightMeters, p2.heightMeters);
            // System.out.print("h ");
            if (p1.heightMeters == p2.heightMeters) {
                return 0;
            }
            if (p1.heightMeters > p2.heightMeters) {
                return 1;
            } else {
                return -1;
            }
        }
    };

}

public class Day02SortPeople {

    static ArrayList<Person> peopleList = new ArrayList<>();

    public static void main(String[] args) {
        peopleList.add(new Person("Jerry", 33, 1.82));
        peopleList.add(new Person("Adam", 53, 1.72));
        peopleList.add(new Person("Tom", 43, 1.62));
        peopleList.add(new Person("Eva", 33, 1.92));
        peopleList.add(new Person("Maria", 22, 1.77));

        System.out.println("======== ORIGINAL INITIAL ORDER");
        for (Person p : peopleList) {
            System.out.println(p);
        }
        //
        Collections.sort(peopleList);
        System.out.println("======== SORTED BY NAME");
        for (Person p : peopleList) {
            System.out.println(p);
        }
        //
        Collections.sort(peopleList, Person.comparatorByAge);
        System.out.println("======== SORTED BY AGE");
        for (Person p : peopleList) {
            System.out.println(p);
        }
        //
        Collections.sort(peopleList, Person.comparatorByHeight);
        System.out.println("======== SORTED BY HEIGHT");
        for (Person p : peopleList) {
            System.out.println(p);
        }
        //
        Collections.sort(peopleList, Person.comparatorByAgeLambdaExample);
        System.out.println("======== SORTED BY AGE USING LAMBDA");
        for (Person p : peopleList) {
            System.out.println(p);
        }
        //
        Collections.sort(peopleList, (Person p1, Person p2) -> p1.age - p2.age );
        System.out.println("======== SORTED BY AGE USING LAMBDA AS PARAMETER");
        for (Person p : peopleList) {
            System.out.println(p);
        }
    }

}
