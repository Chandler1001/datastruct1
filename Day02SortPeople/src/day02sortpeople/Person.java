/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02sortpeople;

import java.util.Comparator;

/**
 *
 * @author Dell
 */
/**
 *
 * @author Dell
 */
public class Person implements Comparable<Person>{

    String name;
    int age;
    double heightMeters;

    public Person(String name, int age, double heightMeters) {
        this.age = age;
        this.heightMeters = heightMeters;
        this.name = name;
    }

    public String toString() {
        return name + "- " + age + "- " + heightMeters;
    }

    public int compareTo(Person comparePerson) {
        return this.name.compareTo(comparePerson.name);
    }

//    public int compareheightMeters(Person comparePerson) {
//        if (this.heightMeters < comparePerson.heightMeters) {
//            return -1;
//        } else if (this.heightMeters > comparePerson.heightMeters) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }


}
