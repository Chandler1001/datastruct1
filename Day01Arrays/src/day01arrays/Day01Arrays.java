/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01arrays;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Dell
 */
public class Day01Arrays {

    static Scanner input = new Scanner(System.in);
    static Random random = new Random();

    public static void main(String[] args) {
        int height;
        int width;
        try {
            System.out.print("How much is your width? ");
            width = input.nextInt();
            System.out.print("How much is your height? ");
            height = input.nextInt();
        } catch (InputMismatchException ex) {
            System.out.println("Error: you must provide an integer number");
            return; // end program
        }
        if (width < 1 || height < 1) {
            System.out.println("Error: width or height must be 1 or greater");
            return; // end program
        }
        int data[][] = new int[height][width];
        System.out.println("----------------Get Col Row Sum------------------");
        getSum(data, height, width);
        System.out.println("----------------Get Deviation--------------------");
        getDeviation(data, height, width);
        System.out.println("----------------Get Pairs----------------------");
        getPairs(data);
    }

    private static void getPairs(int[][] data) {
        ArrayList<Integer> allNumber1 = new ArrayList<>();
        ArrayList<Integer> allNumber2 = new ArrayList<>();
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                allNumber1.add(data[row][col]);
                allNumber2.add(data[row][col]);
            }
        }
        for (int i = 0; i < allNumber1.size(); i++) {
            allNumber2.remove(0);
            for (int j = 0; j < allNumber2.size(); j++) {
                if(isPrime(allNumber1.get(i)+allNumber2.get(j))&&(i!=j)&&(allNumber1.get(i)+allNumber2.get(j))>0){
                     System.out.print("Prime1 : "+allNumber1.get(i)+", Prime2 : "+allNumber2.get(j)+", Prime sum is : "+(allNumber1.get(i)+allNumber2.get(j)));
                     System.out.println();
                }
            }
        }
    }
    static boolean isPrime(int inputNumber) {
        if (inputNumber <= 1) {
            return false;
        }
        boolean isItPrime = true;
        for (int i = 2; i <= inputNumber / 2; i++) {
            if ((inputNumber % i) == 0) {
                return false; // not a prime number
            }
        }
        return true; // not divisible therefore a prime number
    }
    public static void getSum(int[][] data, int height, int width) {
        ArrayList<Integer> colSum = new ArrayList<>();
        for (int i = 0; i < height; i++) {
            int rowSum = 0;
            for (int j = 0; j < width; j++) {
                data[i][j] = random.nextInt(200) - 99; // 1-100
                rowSum += data[i][j];
                System.out.print((j == 0 ? "" : ",") + data[i][j]);
            }
            //columnSum+=data[i][j];
            System.out.print("|| Sum: " + rowSum);
            System.out.println();
        }

        for (int j = 0; j < width; j++) {
            int columnSum = 0;
            for (int i = 0; i < height; i++) {

                columnSum += data[i][j];
            }
            colSum.add(columnSum);
        }
        System.out.println("col sum:"+colSum);
        
    }

    private static void getDeviation(int[][] data, int height, int width) {
        int sumAll = 0;
        int powSum = 0;
        for (int i = 0; i < height; i++) {

            for (int j = 0; j < width; j++) {
                sumAll += data[i][j];
            }
        }
        int Mean = (int) Math.round((double) sumAll / (height * width));
        for (int i = 0; i < height; i++) {

            for (int j = 0; j < width; j++) {
                int sum = data[i][j] - Mean;
                powSum += Math.pow(sum, 2);
            }
        }
        System.out.println(powSum / (height * width));
        System.out.println("Deviation is: " + Math.sqrt(powSum / (height * width)));
    }

    //    public static void main(String[] args) {
//        // TODO code application logic here
//
//        int inputLength;
//        int[] myIntArray = null;
//        try {
//            System.out.println("Enter size of array:");
//            Scanner userInputEntry = new Scanner(System.in);
//            inputLength =userInputEntry.nextInt();
//            myIntArray = new int[inputLength];
//        } catch (InputMismatchException ex) {
//            System.out.println("Invalid input! You have to enter a number");
//        }
//        
//        String arrayLine="";
//        String primeLine="";
//        for (int i = 0; i < myIntArray.length; i++) {
//            int random_int = (int) (Math.random() * (100) + 1); 
//            myIntArray[i]=random_int;
//            arrayLine+=myIntArray[i]+",";
//            boolean flag=true;
//            for (int j = 2; j < random_int-1; j++) {
//                if(random_int%j==0){
//                    flag=false;
//                    break;
//                }
//            }
//            if(flag&&random_int>1){
//                primeLine+=myIntArray[i]+",";
//            }
//        }
//        
//        System.out.println(arrayLine);  
//        System.out.println(primeLine); 
    //}


}
