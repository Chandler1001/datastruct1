/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02sortpeople;

/**
 *
 * @author Dell
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Comparator;

/**
 *
 * @author Dell
 */
/**
 *
 * @author Dell
 */
public class PersonCompare implements Comparator<Person>{


    public int compare(Person t, Person t1) {
        if (t.heightMeters < t1.heightMeters) {
            return -1;
        } else if (t.heightMeters > t1.heightMeters) {
            return 1;
        } else {
            return 0;
        }
    }
}
